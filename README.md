# Projet Hello CSV

Ce projet correspond au deuxieme TD du cours de programmation avanc�e. Il met en avant l'utilisation de git et l'organisation de la documentation


## Exemple

    $ ./ -i helloplus.csv
    
    Hello AJEDDIG  ELIAS ! Hello ALTER  JOHAN ! Hello BRONNER  JEREMY ! Hello JACHIET  FRANCOIS ! Hello FERARY  THOMAS ! Hello MASSICARD  CHARLES !

Photo (copier/coller de la console pour lancer l'application ou photo de ce dernier)

## Auteur
Charles MASSICARD

## Licence
Ce projet est distribu� sous une licence MIT (voir licence.md pour plus de d�tail
